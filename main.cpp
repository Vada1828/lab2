#include <iostream>
#include <cmath>
#include <vector>
#include <string>
#include <cstdlib>

using namespace std;


class Multiplication;

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// class BigInteger begin
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class BigInteger {
private:
    static Multiplication *multiplication;
public:
    vector<int> digit; //base digits of number
    int base = 10; //number system
    int basePower = 3; //amount of digits in one digit
    BigInteger(const char *s = "");

    BigInteger(string current_digits);

    BigInteger(int current_digits);

    BigInteger operator+(BigInteger a);

    BigInteger operator-(BigInteger a);

    BigInteger operator-();

    BigInteger operator*(int a);

    BigInteger operator/(BigInteger a);

    BigInteger operator%(BigInteger a);

    friend BigInteger operator*(BigInteger b, BigInteger a);

    static void setMultiplication(Multiplication *newMult);

    bool operator>=(BigInteger a);

    bool operator<=(BigInteger a);

    bool operator>(BigInteger a);

    bool operator<(BigInteger a);

    bool operator==(BigInteger a);

    bool operator!=(BigInteger a);

    void print(string s = "straight");

    friend ostream &operator<<(ostream &print, const BigInteger &a);
};

Multiplication *BigInteger::multiplication = nullptr;

class Multiplication {
private:
public:
    string multiplication;

    virtual BigInteger multiply(BigInteger b, BigInteger a) = 0;
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void BigInteger::setMultiplication(Multiplication *newMult) {
    multiplication = newMult;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BigInteger abs(BigInteger a) {
    BigInteger c = a;
    c.digit[0] = 0;
    return c;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BigInteger BigInteger::operator/(BigInteger a) {
    BigInteger divide = 0;
    BigInteger b = (*this);
    a = abs(a);
    b = abs(b);
    while (abs((b) - 0) > (abs(a) - 1)) {
        b = b - a;
        divide = divide + 1;
    }
    return divide;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BigInteger BigInteger::operator%(BigInteger a) {
    return (*this) - ((*this) / a) * a;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
BigInteger operator*(BigInteger b, BigInteger a) {
    BigInteger res = BigInteger::multiplication->multiply(b, a);
    return res;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BigInteger::BigInteger(const char *s) {
    string current_digits = s;
    int sign = 0;
    if (current_digits[0] == '-') {
        sign = 1;
        current_digits.erase(0, 1);
    }
    digit.push_back(sign);
    for (int i = current_digits.length() - 1; i >= 0; i -= basePower) {
        int digiti = 0;
        int shift = 1;
        for (int j = 0; j < basePower && i - j >= 0; ++j) {
            digiti += (current_digits[i - j] - '0') * shift;
            shift *= base;
        }
        digit.push_back(digiti);
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BigInteger::BigInteger(string current_digits) {
    int sign = 0;
    if (current_digits[0] == '-') {
        sign = 1;
        current_digits.erase(0, 1);
    }
    digit.push_back(sign);
    for (int i = current_digits.length() - 1; i >= 0; i -= basePower) {
        int digiti = 0;
        int shift = 1;
        for (int j = 0; j < basePower && i - j >= 0; ++j) {
            digiti += (current_digits[i - j] - '0') * shift;
            shift *= base;
        }
        digit.push_back(digiti);
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BigInteger::BigInteger(int current_digits) {
    int sign = 0;
    if (current_digits < 0) {
        sign = 1;
        current_digits = -current_digits;
    }
    digit.push_back(sign);
    for (; current_digits > 0;) {
        int digiti = 0;
        int shift = 1;
        for (int j = 0; j < basePower && current_digits > 0; ++j) {
            digiti += (current_digits % base) * shift;
            shift *= base;
            current_digits /= base;
        }
        digit.push_back(digiti);
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BigInteger BigInteger::operator-() {
    BigInteger c = (*this);
    if (c.digit[0] == 1) c.digit[0] = 0;
    if (c.digit[0] == 0) c.digit[0] = 1;
    return c;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BigInteger BigInteger::operator-(BigInteger a) {
    BigInteger diff; //difference
    if ((a.digit[0] == 1) && (digit[0] == 1)) { return -abs((*this)) + abs(a); }
    if ((a.digit[0] == 1) && (digit[0] == 0)) { return (abs(a) + abs((*this))); }
    if ((a.digit[0] == 0) && (digit[0] == 1)) { return -(abs(a) + abs((*this))); }
    if ((a.digit[0] == 0) && (digit[0] == 0)) {
        if (a == (*this)) return BigInteger(0);
        if (a > (*this)) return -(a - (*this));
        // now we know that *this >= a

        int baseNormal = 1; // base^basePower
        for (int i = 0; i < a.basePower; ++i) baseNormal = baseNormal * a.base;

        int aSize = a.digit.size(), thisSize = digit.size();
        diff.digit.push_back(0);
        for (int i = 1; i < aSize; ++i) {
            diff.digit[i] += (digit[i] - a.digit[i]);
            int overflow = 0;
            if (diff.digit[i] < 0) {
                overflow = -1;
                diff.digit[i] += baseNormal;
            }
            diff.digit.push_back(overflow);
        }

        for (int i = aSize; i < thisSize; ++i) {
            diff.digit[i] += digit[i];
            int overflow = 0;
            if (diff.digit[i] < 0) {
                overflow = -1;
                diff.digit[i] += baseNormal;
            }
            diff.digit.push_back(overflow);
        }
        while (diff.digit[diff.digit.size() - 1] == 0) diff.digit.pop_back();
        return diff;
    }
    return (BigInteger) 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BigInteger BigInteger::operator+(BigInteger a) {
    BigInteger sum = 0;
    if ((a.digit[0] == 1) && (digit[0] == 1)) {
        return -(abs(a) + abs((*this)));
    }
    if ((a.digit[0] == 1) && (digit[0] == 0)) {
        return -abs(a) + abs((*this));
    }
    if ((a.digit[0] == 0) && (digit[0] == 1)) {
        return abs(a) - abs((*this));
    }

    if ((a.digit[0] == 0) && (digit[0] == 0)) {
        int baseNormal = 1; // base^basePower
        for (int i = 0; i < a.basePower; ++i) baseNormal = baseNormal * a.base;
        int minDigit, maxDigit, maxNumber; // maxNuber == 1 if a>this; maxNuber == 0 if a<=this

        if (a.digit.size() > digit.size()) {
            minDigit = digit.size();
            maxDigit = a.digit.size();
            maxNumber = 1;
        } else {
            minDigit = a.digit.size();
            maxDigit = digit.size();
            maxNumber = 0;
        }

        sum.digit.push_back(0);
        for (int i = 1; i < minDigit; ++i) {
            sum.digit[i] += (a.digit[i] + digit[i]);
            int overflow = sum.digit[i] / baseNormal;
            sum.digit[i] = sum.digit[i] % baseNormal;
            sum.digit.push_back(overflow);
        }

        if (maxNumber == 1) {
            for (int i = minDigit; i < maxDigit; ++i) {
                sum.digit[i] += (a.digit[i]);
                int overflow = sum.digit[i] / baseNormal;
                sum.digit[i] = sum.digit[i] % baseNormal;
                sum.digit.push_back(overflow);
            }
        } else {
            for (int i = minDigit; i < maxDigit; ++i) {
                sum.digit[i] += (digit[i]);
                int overflow = sum.digit[i] / baseNormal;
                sum.digit[i] = sum.digit[i] % baseNormal;
                sum.digit.push_back(overflow);
            }
        }

        if (sum.digit[sum.digit.size() - 1] == 0) sum.digit.pop_back();
        return sum;
    }
    return (BigInteger) 0;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool BigInteger::operator<(BigInteger a) {
    // = 1 if a > b; = 0 if a <=b
    if (((*this) > a) || ((*this) == a)) return false;
    else return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool BigInteger::operator>(BigInteger a) {
    if ((a.digit[0] == 1) && (digit[0] == 1)) {
        return abs((*this)) > abs(a);
    }
    if ((a.digit[0] == 1) && (digit[0] == 0)) return true;
    if ((a.digit[0] == 0) && (digit[0] == 1)) return false;
    if ((a.digit[0] == 0) && (digit[0] == 0)) {
        if (a.digit.size() < digit.size()) return true;
        if (a.digit.size() > digit.size()) return false;
        for (int i = a.digit.size() - 1; i >= 1; --i) {
            if (a.digit[i] < digit[i]) return true;
            if (a.digit[i] > digit[i]) return false;
        }
    }
    return false;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool BigInteger::operator>=(BigInteger a) {
    return (a < (*this));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool BigInteger::operator<=(BigInteger a) {
    return (a > (*this));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool BigInteger::operator!=(BigInteger a) {
    return !(a == (*this));
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool BigInteger::operator==(BigInteger a) {
    if ((a.digit[0] == 1) && (digit[0] == 1)) {
        return abs((*this)) == abs(a);
    }
    if ((a.digit[0] == 1) && (digit[0] == 0)) return true;
    if ((a.digit[0] == 0) && (digit[0] == 1)) return false;
    if ((a.digit[0] == 0) && (digit[0] == 0)) {
        if (a.digit.size() != digit.size()) return false;
        for (int i = a.basePower - 1; i >= 1; --i) {
            if (a.digit[i] < digit[i]) return false;
            if (a.digit[i] > digit[i]) return false;
        }
    }
    return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ostream &operator<<(ostream &print, const BigInteger &a) {
    if (a.digit[0]) print << "-";
    print << a.digit[a.digit.size() - 1];
    int basePower = a.basePower;
    int size = a.digit.size();
    for (int i = size - 2; i > 0; --i) {
        int shift = a.base;
        int breaker = 0;
        for (int j = 1; j < basePower; j++) {
            if ((breaker == 0) && (a.digit[i] < shift)) {
                breaker = 1;
                for (int k = 0; k < basePower - j; ++k) {
                    print << '0';
                }
            }
            shift *= a.base;
        }
        print << a.digit[i];
    }
    return print;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void BigInteger::print(string s) {
    if (s == "reverse") {
        if (digit[0]) { cout << "(1-) "; } else if (digit[0] == 0) { cout << "(0+) "; }
        for (int i = 1; i <= digit.size() - 1; ++i) {
            cout << digit[i] << " ";
        }
        cout << endl;
    } else if (s == "straight") {
        if (digit[0]) { cout << "(1-) "; } else if (digit[0] == 0) { cout << "(0+) "; }
        for (int i = digit.size() - 1; i >= 1; --i) {
            cout << digit[i] << " ";
        }
        cout << endl;
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

BigInteger BigInteger::operator*(int a) {
    BigInteger mult = 0;
    int adigit;
    if (a < 0) adigit = -1;
    else adigit = 1;
    a = abs(a);
    int baseNormal = 1; // base^basePower
    for (int i = 0; i < basePower; ++i) baseNormal = baseNormal * base;
    int thisSize = digit.size();
    int moverflow = 0;
    mult.digit.push_back(0);
    for (int i = 1; i < thisSize; ++i) {
        mult.digit[i] += (digit[i] * a);
        int overflow = mult.digit[i] / baseNormal;
        mult.digit[i] = mult.digit[i] % baseNormal;
        if (i != thisSize - 1) mult.digit.push_back(overflow);
        else moverflow = overflow;
    }
    while (moverflow) {
        mult.digit.push_back(moverflow % baseNormal);
        moverflow = moverflow / baseNormal;
    }
    if (mult.digit[mult.digit.size() - 1] == 0) mult.digit.pop_back();
    if ((adigit == 1) && (digit[0] == 0)) { mult.digit[0] = 0; }
    else if ((adigit == 1) && (digit[0] == 1)) { mult.digit[0] = 1; }
    else if ((adigit == -1) && (digit[0] == 0)) { mult.digit[0] = 1; }
    else if ((adigit == -1) && (digit[0] == 1)) { mult.digit[0] = 0; }
    return mult;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// class BigInteger end
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Normal_Multiplication : public Multiplication {
public:
    BigInteger multiply(BigInteger b, BigInteger a) {
        BigInteger mult = 0;

        int baseNormal = 1; // base^basePower
        for (int i = 0; i < a.basePower; ++i) baseNormal = baseNormal * a.base;

        int thisSize = b.digit.size(), aSize = abs(a).digit.size();
        mult.digit.push_back(0);
        vector<int> shift;
        for (int i = 1; i < aSize; ++i) {
            BigInteger summand = abs(b) * abs(a).digit[i];
            summand.digit.insert(summand.digit.begin(), shift.begin(), shift.end());
            shift.push_back(0);
            mult = mult + summand;
        }

        if ((a.digit[0] == 0) && (b.digit[0] == 0)) { mult.digit[0] = 0; }
        else if ((a.digit[0] == 0) && (b.digit[0] == 1)) { mult.digit[0] = 1; }
        else if ((a.digit[0] == 1) && (b.digit[0] == 0)) { mult.digit[0] = 1; }
        else if ((a.digit[0] == 1) && (b.digit[0] == 1)) { mult.digit[0] = 0; }

        return mult;
    }
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Kararsuba_Multiplication : public Multiplication {
public:
    BigInteger multiply(BigInteger b, BigInteger a) {
        BigInteger mult;
        if ((a.digit.size() <= 2)) return b * a.digit[1] * ((-b.digit[0]) * 2 + 1);
        else if ((b.digit.size() <= 2)) return a * b.digit[1] * ((-a.digit[0]) * 2 + 1);
        else {
            int baseNormal = 1; // base^basePower
            for (int i = 0; i < a.basePower; ++i) baseNormal = baseNormal * a.base;
            BigInteger x_1, x_2, y_1, y_2;
            // creating
            int n = max((a.digit.size() - 1) / 2, (b.digit.size() - 1) / 2);
            for (int i = 1; i <= n; ++i)
                if (i < a.digit.size()) x_2.digit.push_back(a.digit[i]);
                else x_2.digit.push_back(0);
            for (int i = n + 1; i < a.digit.size(); ++i)
                if (i < a.digit.size()) x_1.digit.push_back(a.digit[i]);
                else x_1.digit.push_back(0);

            for (int i = 1; i <= n; ++i)
                if (i < b.digit.size()) y_2.digit.push_back(b.digit[i]);
                else y_2.digit.push_back(0);
            for (int i = n + 1; i < b.digit.size(); ++i)
                if (i < b.digit.size()) y_1.digit.push_back(b.digit[i]);
                else y_1.digit.push_back(0);
            BigInteger c_1, c_2, c_3, c_add = y_1 + y_2;
            c_1 = x_1 * y_1;
            c_2 = x_2 * y_2;
            c_3 = (x_1 + x_2) * (y_1 + y_2) - c_1 - c_2;
            for (int i = 0; i < n; ++i)
                c_3 = c_3 * baseNormal;
            for (int i = 0; i < 2 * n; ++i)
                c_1 = c_1 * baseNormal;
            mult = c_1 + c_2 + c_3;
            if ((a.digit[0] == 0) && (b.digit[0] == 0)) { mult.digit[0] = 0; }
            else if ((a.digit[0] == 0) && (b.digit[0] == 1)) { mult.digit[0] = 1; }
            else if ((a.digit[0] == 1) && (b.digit[0] == 0)) { mult.digit[0] = 1; }
            else if ((a.digit[0] == 1) && (b.digit[0] == 1)) { mult.digit[0] = 0; }
        }
        return mult;
    }
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
class Toom_Cook : public Multiplication {
public:
    BigInteger multiply(BigInteger b, BigInteger a) {
        BigInteger mult;
        if ((a.digit.size() <= 2)) return b * a.digit[1] * ((-b.digit[0]) * 2 + 1);
        else if ((b.digit.size() <= 2)) return a * b.digit[1] * ((-a.digit[0]) * 2 + 1);
        else {
            int baseNormal = 1; // base^basePower
            for (int i = 0; i < a.basePower; ++i) baseNormal = baseNormal * a.base;
            if ((a.digit.size() <= 3)) return (b * a.digit[1] + b * a.digit[2] * baseNormal) * ((-a.digit[0]) * 2 + 1);
            if ((a.digit.size() <= 3)) return (a * b.digit[1] + a * b.digit[2] * baseNormal) * ((-b.digit[0]) * 2 + 1);
            BigInteger x_1, x_2, x_3, y_1, y_2, y_3;
            // creating
            int n = max(max(max((a.digit.size()) / 3, (a.digit.size() + 1) / 3), (a.digit.size() - 1) / 3),
                        max(max((b.digit.size()) / 3, (b.digit.size() + 1) / 3), (b.digit.size() - 1) / 3));
            for (int i = 1; i <= n; ++i)
                if (i < a.digit.size()) x_3.digit.push_back(a.digit[i]);
                else x_3.digit.push_back(0);
            for (int i = n + 1; i <= 2 * n; ++i)
                if (i < a.digit.size()) x_2.digit.push_back(a.digit[i]);
                else x_2.digit.push_back(0);
            for (int i = 2 * n + 1; i < a.digit.size(); ++i)
                if (i < a.digit.size()) x_1.digit.push_back(a.digit[i]);
                else x_1.digit.push_back(0);

            for (int i = 1; i <= n; ++i)
                if (i < b.digit.size()) y_3.digit.push_back(b.digit[i]);
                else y_3.digit.push_back(0);
            for (int i = n + 1; i <= 2 * n; ++i)
                if (i < b.digit.size()) y_2.digit.push_back(b.digit[i]);
                else y_2.digit.push_back(0);
            for (int i = 2 * n + 1; i < a.digit.size(); ++i)
                if (i < b.digit.size()) y_1.digit.push_back(b.digit[i]);
                else y_1.digit.push_back(0);
            // cout << "x_1:" << x_1 << endl<< "x_2:" << x_2 << endl<< "x_3:" << x_3 << endl;
            // cout << "y_1:" << y_1 << endl<< "y_2:" << y_2 << endl<< "y_3:" << y_3 << endl;
            BigInteger c_0, c_1, c_2, c_3, c_4;
            c_0 = x_3 * y_3;
            c_1 = (x_1 + x_2 + x_3) * (y_1 + y_2 + y_3);
            c_2 = (x_1 * 4 + x_2 * 2 + x_3) * (y_1 * 4 + y_2 * 2 + y_3);
            c_3 = (x_1 * 9 + x_2 * 3 + x_3) * (y_1 * 9 + y_2 * 3 + y_3);
            c_4 = (x_1 * 16 + x_2 * 4 + x_3) * (y_1 * 16 + y_2 * 4 + y_3);
            BigInteger f_0, f_1, f_2, f_3, f_4;
            f_0 = c_0;
            f_1 = c_1 - f_0;
            f_2 = c_2 - c_1 - f_1; // *2
            f_3 = (c_3 - c_2 - (c_2 - c_1)) - f_2; //*6
            f_4 = (c_4 - c_3 - (c_3 - c_2) - (c_3 - c_2 - (c_2 - c_1))) - f_3; //*24
            f_1 = f_1 * baseNormal;
            f_2 = f_2 * ((baseNormal * (baseNormal - 1)) / 2);
            f_3 = f_3 * ((baseNormal * (baseNormal - 1) * (baseNormal - 2)) / 6);
            f_4 = f_4 * ((baseNormal * (baseNormal - 1) * (baseNormal - 2) * (baseNormal - 3)) / 24);
            mult = f_0 + f_1 + f_2 + f_3 + f_4;

            if ((a.digit[0] == 0) && (b.digit[0] == 0)) { mult.digit[0] = 0; }
            else if ((a.digit[0] == 0) && (b.digit[0] == 1)) { mult.digit[0] = 1; }
            else if ((a.digit[0] == 1) && (b.digit[0] == 0)) { mult.digit[0] = 1; }
            else if ((a.digit[0] == 1) && (b.digit[0] == 1)) { mult.digit[0] = 0; }
        }
        return mult;
    }
};

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

int GCD(int a, int b) {
    if (a < b)
        return GCD(b, a);
    else if (a % b == 0)
        return b;
    else return GCD(b, a % b);
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool isMTF(int a, int n) {
    // a^n-1 mod n
    if (GCD(a, n) != 1) return false;  //can we use MTF
    int apow = 1; // a^(n-1)
    for (int i = 0; i < n - 1; ++i) {
        apow *= a;
        apow %= n;
    }
    if (apow != 1) return false;  //if a^(n-1) != 1 (mod n)
    return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

bool isPrime_MTF(int n, int k = 100) { // isPrime for n; k is iteration number
    if (n == 1) return false;
    if (n == 2) return true;
    vector<int> a;
    for (int i = 0; i < k; ++i) a.push_back(rand() % (n - 3) + 2);
    //for (int i = 2; i < n - 1; ++i) a.push_back(i); //for 100%
    //random_shuffle(a.begin(), a.end());
    for (int i = 0; i < n - 3; ++i) {
        if (!isMTF(a[i], n)) {
            return false;
        }
    }
    return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int a_deg_k_mod_n(int a, int k, int n) {
    // a^k mod n
    if (k == 0) return 1;
    if (k == 1) return a;
    int apow = 1; // a^k
    for (int i = 0; i < k; ++i) {
        apow *= a;
        apow %= n;
    }
    return apow;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool isPrime_Miller_Rabin(int n, int k = 1000) {
    if (n == 1) return false;
    if (n == 2) return true;
    if (n % 2 == 0) return false;
    int s = 0, d = n - 1;
    while (d % 2 == 0) {
        d /= 2;
        s++;
    }
    int y;
    for (int i = 0; i < k; ++i) {
        int a = rand() % (n - 3) + 2;
        int x = a_deg_k_mod_n(a, d, n);

        for (int j = 0; j < s; ++j) {
            y = a_deg_k_mod_n(x, 2, n);
            if ((y == 1) && (x != 1) && (x != n - 1))
                return false;
            x = y;
        }
        if (y != 1)
            return false;
    }
    return true;
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int Jacobi_symbol (int a, int n){
    //(a/n)
    if (n % 2 == 0) return (int) 0;
    if (a < 0) return -(Jacobi_symbol ( -a,  n));
    a %= n;
    int result = 1;
    while (a != 0) {
        while (a % 2 == 0) {
            a /= 2; //divide 2 from a
            int r = n % 8;
            if (r == 3 || r == 5)
                result = -result; // quadratic residue theorem for 2 (for dividing 2 from a)
        }
        int swap = a; //swap a, n
        a = n;
        n = swap;
        if (a % 4 == 3 && n % 4 == 3) result = -result; // quadratic residue theorem by swap
        a %= n;
    }
    if (n == 1) return result;
    else return (int) 0;
}


//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
bool isPrime_Solovay_Strassen(int n, int k = 1000) {
//by quadratic residue method
    if (n == 1) return false;
    if (n == 2) return true;
    if (n % 2 == 0) return false;
    for (int i = 0; i < k; ++i) {
        int a = rand() % (n - 3) + 2;
        int x = Jacobi_symbol(a,n);
        if ((x == 0)||(a_deg_k_mod_n(a,((n-1)/2),n)!=((x+n)%n))) return false;
    }
    return true;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

void TEST() {
    BigInteger a, b;
    //ADD tester
    //1 Test
    b = "99977";
    a = "-99999";
    cout << "Addition test:" << a << " + " << b << endl;
    cout << "   ";
    a.print();
    cout << " + ";
    b.print();
    cout << " = ";
    cout << a + b << endl;
    // 2 test
    b = "100100";
    a = 77799;
    cout << "Addition test:" << a << " + " << b << endl;
    cout << "   ";
    a.print();
    cout << " + ";
    b.print();
    cout << " = ";
    cout << a + b << endl;
    // 3 test
    b = "100300";
    a = -77799;
    cout << "Addition test:" << a << " + " << b << endl;
    cout << "   ";
    a.print();
    cout << " + ";
    b.print();
    cout << " = ";
    cout << a + b << endl;
    // 4 test
    b = "-77";
    a = "-77768534599";
    cout << "Addition test:" << a << " + " << b << endl;
    cout << "   ";
    a.print();
    cout << " + ";
    b.print();
    cout << " = ";
    cout << a + b << endl;
    // 5 test
    b = 700067;
    a = "122312";
    cout << "Addition test:" << a << " + " << b << endl;
    cout << "   ";
    a.print();
    cout << " + ";
    b.print();
    cout << " = ";
    cout << a + b << endl;
    // 5 test
    b = "0";
    a = "122312";
    cout << "Addition test:" << a << " + " << b << endl;
    cout << "   ";
    a.print();
    cout << " + ";
    b.print();
    cout << " = ";
    cout << a + b << endl;
    cout << "____________________________" << endl;

    //DIFF tester
    //1 Test
    b = "99977";
    a = "-99999";
    cout << "Difference test:" << a << " - " << b << endl;
    cout << "   ";
    a.print();
    cout << " - ";
    b.print();
    cout << " = ";
    cout << a - b << endl;
    // 2 test
    b = "100100";
    a = "77799";
    cout << "Difference test:" << a << " - " << b << endl;
    cout << "   ";
    a.print();
    cout << " - ";
    b.print();
    cout << " = ";
    cout << a - b << endl;
    // 3 test
    b = "100300";
    a = "-77799";
    cout << "Difference test:" << a << " - " << b << endl;
    cout << "   ";
    a.print();
    cout << " - ";
    b.print();
    cout << " = ";
    cout << a - b << endl;
    // 4 test
    b = "-77";
    a = "-77768534599";
    cout << "Difference test:" << a << " - " << b << endl;
    cout << "   ";
    a.print();
    cout << " - ";
    b.print();
    cout << " = ";
    cout << a - b << endl;
    // 5 test
    b = "700067";
    a = "122312";
    cout << "Difference test:" << a << " - " << b << endl;
    cout << "   ";
    a.print();
    cout << " - ";
    b.print();
    cout << " = ";
    cout << a - b << endl;
    // 6 test
    b = 0;
    a = "122312";
    cout << "Difference test:" << a << " - " << b << endl;
    cout << "   ";
    a.print();
    cout << " - ";
    b.print();
    cout << " = ";
    cout << a - b << endl;
    cout << "____________________________" << endl;


    //Multiplication tester
    //1 Test
    b = "99977";
    a = "-99999";
    cout << "Multiplication test:" << a << " * " << b << endl;
    cout << "   ";
    a.print();
    cout << " * ";
    b.print();
    cout << " = ";
    cout << a * b << endl;
    // 2 test
    b = "100100";
    a = "77799";
    cout << "Multiplication test:" << a << " * " << b << endl;
    cout << "   ";
    a.print();
    cout << " * ";
    b.print();
    cout << " = ";
    cout << a * b << endl;
    // 3 test
    b = "100300";
    a = "-77799";
    cout << "Multiplication test:" << a << " * " << b << endl;
    cout << "   ";
    a.print();
    cout << " * ";
    b.print();
    cout << " = ";
    cout << a * b << endl;
    // 4 test
    b = "-77";
    a = "-77768534599";
    cout << "Multiplication test:" << a << " * " << b << endl;
    cout << "   ";
    a.print();
    cout << " * ";
    b.print();
    cout << " = ";
    cout << a * b << endl;
    // 5 test
    b = "700067";
    a = "122312";
    cout << "Multiplication test:" << a << " * " << b << endl;
    cout << "   ";
    a.print();
    cout << " * ";
    b.print();
    cout << " = ";
    cout << a * b << endl;
    // 6 test
    b = "0";
    a = "122312";
    cout << "Multiplication test:" << a << " * " << b << endl;
    cout << "   ";
    a.print();
    cout << " * ";
    b.print();
    cout << " = ";
    cout << a * b << endl;
    cout << "____________________________" << endl;


    // MTF tester
    int MTFmethod = 991; //7 1 10 12378 16 2 3 953 587 991
    cout << "MTF Prime test for: " << MTFmethod << endl;
    if (isPrime_MTF(MTFmethod, 1000)) cout << "Maybe prime" << endl;
    else cout << "Composite" << endl;
    cout << "____________________________" << endl;

    // Miller-Rabin tester
    int MRmethod = 12378; //7 1 10 12378 16 2 3 953 587 991
    cout << "Miller-Rabin Prime test for: " << MTFmethod << endl;
    if (isPrime_Miller_Rabin(MTFmethod, 1000)) cout << "Maybe prime" << endl;
    else cout << "Composite" << endl;
    cout << "____________________________" << endl;


    // Solovay Strassen tester
    int SSmethod = 12378; //7 1 10 12378 16 2 3 953 587 991
    cout << "Solovay Strassen Prime test for: " << MTFmethod << endl;
    if (isPrime_Solovay_Strassen(MTFmethod, 1000)) cout << "Maybe prime" << endl;
    else cout << "Composite" << endl;
    cout << "____________________________" << endl;
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
int main() {
    //BigInteger::setMultiplication(new Toom_Cook());
    //BigInteger::setMultiplication(new Kararsuba_Multiplication());
    BigInteger::setMultiplication(new Normal_Multiplication());
    TEST();
    return 0;
}